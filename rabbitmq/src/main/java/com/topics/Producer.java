package com.topics;

import com.one2many.ConnectionUtil;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class Producer {
    private static final String EXCHANGE_NAME = "test_exchange_topic";

    public static void main(String[] args) throws Exception {
        /*Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel(1);
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String message = "匹配消息";
        channel.basicPublish(EXCHANGE_NAME,"order.update",false,false,null,message.getBytes());
        channel.close();
        connection.close();*/
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME,"topic");
        String message = "匹配insert";
        channel.basicPublish(EXCHANGE_NAME,"order.update",false,false,null,message.getBytes());

        channel.close();
        connection.close();

    }
}
