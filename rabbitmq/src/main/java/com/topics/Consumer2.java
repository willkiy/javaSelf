package com.topics;

import com.one2many.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer2 {
    public static String EXCHANGE_NAME = "test_exchange_topic";
    public static String QUEUE_NAME = "test_queue_topic_2";

    public static void main(String[] args) throws Exception {
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel(1);
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        channel.exchangeDeclare(EXCHANGE_NAME,"topic");
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME,"order.insert");
        channel.basicQos(1);
        Consumer consumer = new DefaultConsumer(channel){
          @Override
          public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
              super.handleDelivery(consumerTag, envelope, properties, body);
              System.out.println("接收消息：" + new String(body, "UTF-8"));
          }
        };
    }
}
