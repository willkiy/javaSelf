package com.one2many;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class ProducerWorker {
    public static String QUEUE_NAME = "test_queue_work";
    public static void main(String []args) throws  Exception{
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel(1);
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        for(int i=0;i<=100;i++){
            String message = "helloworld"+i;
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
            Thread.sleep(100*i);

        }
        channel.close();
        connection.close();
    }

}
