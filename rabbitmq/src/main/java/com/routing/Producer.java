package com.routing;

import com.one2many.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class Producer {
    public static String EXCHANGE_NAME = "test_exchange_direct1";

    public static void main(String[] args)   throws  Exception{
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel(1);
        channel.exchangeDeclare(EXCHANGE_NAME, "direct");
        // 消息内容
        String message = "这是消息B";
        channel.basicPublish(EXCHANGE_NAME,"B",null,message.getBytes());
        System.out.println(" [生产者] Sent B'" + message + "'");
        message = "这是消息A";
        channel.basicPublish(EXCHANGE_NAME,"A",null,message.getBytes());
        System.out.println(" [生产者] Sent A'" + message + "'");
        channel.close();
        connection.close();
    }
}
