package com.routing;

import com.one2many.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

public class Consumer1 {
    public final static String EXCHANGE_NAME = "test_exchange_direct1";
    public final static String QUEUE_NAME = "test_queue_direct_A";
    public static void main(String[] args) throws Exception {
        Connection connection = ConnectionUtil.getConnection();
        Channel channel = connection.createChannel(1);
        channel.queueDeclare(QUEUE_NAME,false, false, false, null);
        channel.exchangeDeclare(EXCHANGE_NAME,"direct");
        channel.queueBind(QUEUE_NAME,EXCHANGE_NAME,"A");
        channel.basicQos(1);
        // 定义队列的消费者
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                super.handleDelivery(consumerTag, envelope, properties, body);
                System.out.println(new String(body,"UTF-8"));
            }
        };
        channel.basicConsume(QUEUE_NAME,true,consumer);
    }
}
