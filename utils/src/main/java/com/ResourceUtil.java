package com;

import java.util.ResourceBundle;



/**
 * 项目参数工具类
 * 
 */
public class ResourceUtil {
	
	private static final ResourceBundle bundle = ResourceBundle.getBundle("sysConfig");

	/**
	 * 获取session定义名称
	 * 
	 * @return
	 */
	public static final String getSessionattachmenttitle(String sessionName) {
		return bundle.getString(sessionName);
	}
	
}
