package com;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    // 转换成十位位日期格式
    public static String DateConversion10String(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String str = sdf.format(date);
        return str;
    }

    // 转换成八位位日期格式
    public static String DateConversion8String(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String str = sdf.format(date);
        return str;
    }

    /**
     * 将时间格式的数据转换成14位（yyyymmddHHmmss）
     *
     * @param date
     *            Date类型
     * @return
     */
    public static String DateConversion14String(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String str = sdf.format(date);
        return str;
    }

    // 获取当前时间的毫秒数
    public static String getDatecurrentTime() {
        long time = System.currentTimeMillis();
        String datetime = new Long(time).toString();
        return datetime;
    }

    /**
     * 将时间格式的数据转换成8位（yyyyMMdd）
     *
     * @param date
     *            String类型
     * @return
     */
    public static String StringConversionString(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String str = null;
        try {
            str = sdf.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 将时间格式的数据转换成14位（yyyymmddHHmmss）
     *
     * @param date
     *            String类型
     * @return
     */
    public static String StringConversion14String(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String str = null;
        try {
            str = sdf.format(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 将字符串转化成Date
     *
     * @param dateString
     *            String类型
     * @return
     */
    public static Date StringConversionDate(String dateString) {
        Date str = null;
        SimpleDateFormat sdf = null;
        if (dateString.length() == 8) {
            sdf = new SimpleDateFormat("yyyyMMdd");
        } else if (dateString.length() == 10) {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
        } else if (dateString.length() == 14) {
            sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        } else if (dateString.length() == 19) {
            dateString = dateString.replace("T", " ").replaceAll("/", "-");
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        } else {
            sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
        }
        try {
            str = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    //时间差计算转换成天数
    public static int daysBetween(String smdate,String bdate){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(smdate));
            long time1 = cal.getTimeInMillis();
            cal.setTime(sdf.parse(bdate));
            long time2 = cal.getTimeInMillis();
            long between_days = (time2 - time1) / (1000 * 3600 * 24);

            return Integer.parseInt(String.valueOf(between_days));
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
    public static Date getNextDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        date = calendar.getTime();
        return date;
    }

    public static String getYestoDay() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return getDefautlString(date);
    }
    /**
     * 默认格式 yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String getDefautlString(Date date) {

        String parttern = "yyyy-MM-dd";

        return getString(date, parttern);
    }
    public static String getString(Date date, String pattern) {

        if (date == null)
            throw new IllegalArgumentException("date is null");
        if (pattern == null)
            throw new IllegalArgumentException("pattern is null");

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        return sdf.format(date);
    }
    /**
     * 获取 月最后一天
     *
     * @param n
     * @return
     */
    public static String getMonLastDay(String day) {
        // 获取Calendar
        Date date = null;
        date = getAppointDate(day);
        if (date == null) {
            date = new Date();
        }
        Calendar calendar = Calendar.getInstance();
        // 设置Calendar月份数为下一个月
        calendar.setTime(date);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
        calendar.set(Calendar.DATE, 1);
        calendar.add(Calendar.DATE, -1);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String lastday = format.format(calendar.getTime());
        return lastday;
    }
    /**
     * 返回日期指定时间 默认格式 yyyy-MM-dd
     *
     * @param date
     *            格式化出异常 格式化成 2099-12-31
     * @return
     */
    public static Date getAppointDate(String date) {
        Date tmp = null;
        if (date == null || date.equals(""))
            throw new IllegalArgumentException("date is null");
        String parttern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(parttern);
        try {
            ParsePosition pos = new ParsePosition(0);
            // String da=sdf.format(date);
            tmp = sdf.parse(date, pos);
        } catch (Exception e) {
            try {
                tmp = sdf.parse("2099-12-31");
                return tmp;
            } catch (ParseException e1) {
                e1.printStackTrace();
                return null;
            }
        }
        return tmp;
    }
    /**
     * 获取 月 的第一天 n=0当前月 ，n=-1上个月 n=-2上上个月
     *
     * @param n
     * @return
     */
    public static String getMonFirstDay(int n) {
        // 获取Calendar
        Calendar calendar = Calendar.getInstance();
        // 设置Calendar月份数为下一个月
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - n);
        // 设置Calendar日期为下一个月一号
        calendar.set(Calendar.DATE, 1);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday = format.format(calendar.getTime());
        System.out.println("firstday:" + firstday);
        return firstday;
    }
    public static int getYear() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        return year;
    }

    public static int getLastYear(int last) {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR) - last;
        return year;
    }


}
